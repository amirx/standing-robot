import RPi.GPIO as GPIO            # import RPi.GPIO module
from time import sleep             # lets us have a delay
GPIO.setmode(GPIO.BCM)             # choose BCM or BOARD
GPIO.setup(6, GPIO.OUT)           # set GPIO24 as an output
GPIO.setup(12, GPIO.OUT)           # set GPIO24 as an output
GPIO.setup(20, GPIO.OUT)           # set GPIO24 as an output
GPIO.setup(26, GPIO.OUT)           # set GPIO24 as an output


try:
    GPIO.output(6, 0);
    GPIO.output(12, 0);
    GPIO.output(20, 0);
    GPIO.output(26, 0);
    GPIO.cleanup()                 # resets all GPIO ports used by this program
#        sleep(0.5)                 # wait half a second

except KeyboardInterrupt:          # trap a CTRL+C keyboard interrupt
    GPIO.cleanup()                 # resets all GPIO ports used by this program


