/*  Arduino DC Motor Control - PWM | H-Bridge | L298N  -  Example 01
    by Dejan Nedelkovski, www.HowToMechatronics.com
*/
#include "mpu_driver.h"

#define enA 11
#define in1A 3
#define in2A 4
#define enB 10
#define in1B 5
#define in2B 6

const int motorA[4] = {enA, in1A, in2A, 10};
const int motorB[4] = {enB, in1B, in2B, 10};
int prev_time;

int rotDirection = 0;
int pressed = false;
void setup_motor() {
  pinMode(enA, OUTPUT);
  pinMode(in1A, OUTPUT);
  pinMode(in2A, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in1B, OUTPUT);
  pinMode(in2B, OUTPUT);
  // Set initial rotation direction
  digitalWrite(in1A, LOW);
  digitalWrite(in2A, HIGH);
  digitalWrite(in1B, LOW);
  digitalWrite(in2B, HIGH);
}

void setup(){
  setup_mpu();
  setup_motor();
}
void setMotorSpeed(const int motor[4], int power){
  int motor_minimum_power = motor[3];
  if (power > 0){
    analogWrite(motor[0], power + motor_minimum_power); // Send PWM signal to L298N Enable pin
    digitalWrite(motor[1], LOW);
    digitalWrite(motor[2], HIGH);
  }else if (power < 0){
    analogWrite(motor[0], -power + motor_minimum_power); // Send PWM signal to L298N Enable pin
    digitalWrite(motor[1], HIGH);
    digitalWrite(motor[2], LOW);
  }else {
    analogWrite(motor[0], 0); // Send PWM signal to L298N Enable pin
    digitalWrite(motor[1], LOW);
    digitalWrite(motor[2], LOW);
  }
}

int count = 0;
int prev_degrees = 0;
float sum_degrees = 0;
float kp = 4;
float kd = 300;
float ki = 2;

void loop() {
  
  process_mpu();
   
  int cur_time = millis();
  int delta_time = cur_time - prev_time;
  float cur_degrees = ypr[1]*180/M_PI;
  float delta_degrees = cur_degrees - prev_degrees;
  sum_degrees += cur_degrees*delta_time/1000.0;
    
  float pterm = cur_degrees*kp/10;
  float dterm = delta_degrees/delta_time*kd;
  float iterm = sum_degrees*ki;

  int power = pterm + dterm + iterm;
  
  setMotorSpeed(motorA,power);
  setMotorSpeed(motorB,power);

  Serial.print(delta_time); Serial.print("\t"); Serial.print(power); Serial.print("\t"); Serial.print(pterm); Serial.print("\t"); Serial.print(dterm); Serial.print("\t"); Serial.print(iterm); Serial.print(" kp kd ki\t"); 
  Serial.print(kp); Serial.print("\t"); Serial.print(kd); Serial.print("\t"); Serial.print(ki); Serial.print("\n"); 

  process_input();
  
  prev_time = cur_time;
  prev_degrees = cur_degrees;
}

void process_input(){
  if (Serial.available()){
    int r = Serial.read();
    if(r == 'q'){
      kp *= 1.1;
    }
    if(r == 'a'){
      kp /= 1.1;
    }
    if(r == 'w'){
      kd *= 1.1;
    }
    if(r == 's'){
      kd /= 1.1;
    }
    if(r == 'e'){
      ki *= 1.1;
    }
    if(r == 'd'){
      ki /= 1.1;
    }
    if(r == 'Q'){
      kp *= 2;
    }
    if(r == 'A'){
      kp /= 2;
    }
    if(r == 'W'){
      kd *= 2;
    }
    if(r == 'S'){
      kd /= 2;
    }
    if(r == 'E'){
      ki *= 2;
    }
    if(r == 'D'){
      ki /= 2;
    }
  }
}
